# University of Melbourne
# School of Engineering
# SWEN90013 Masters Advanced Software Project - 2019
# Team IC

FROM python:3.7.4-slim-buster

# COPY Necessary documents and folders
WORKDIR "/app/app/sites"
COPY sites/ .

