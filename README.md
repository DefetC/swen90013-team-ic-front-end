# TEAM-IC-FRONT-END

This repo is about the front-end part of Integrity checking project from Software Project (COMP90082_2020_SM2) at University of Melbourne, 2020..

This repo contains three different sites for different usages: instructor, student and unauthorized sites.

This website is built using Angular library along with SCSS package.

# Build repository with Pipeline on Gitlab

1. Firstly you need to browse DevOps repository and set up DevOps environment on AWS server.
2. Create a new repository on Gitlab. Upload the source code to this repository.
3. The pipeline of Gitlab will automatically run, and build the repository followed by the instruction of ".gitlab-ci.yml".
4. After pipeline finish, we can get the container package in the Container Registry(Packages & Registries->Container Registry). It will be used in the setup of the WebAPI repository.
   1. we can get the **link to the container package**. It is "registry.gitlab.com/name of your account/name of your repository".
   2. For example "registry.gitlab.com/defetc/swen90013-team-ic-front-end"



# Build repository locally

NOTE: Local installation methods are legacy methods left by previous teams. We strongly recommend that you use pipeline to install project in AWS server instead.

## Notification

The main folder for this application is `authorized-site`, all the commands below should be executed within this folder, so you should first run `cd authorized-site`

## Environment Setup

Run `npm install` to install dependencies:

## Development server

Run `ng serve`  for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

You can set base URL for your deployment server in `src/environments/environment.prod.ts` file.

Run `ng build` to build the project. The build artifacts will be stored in the `sites/` directory which can be used for deployment . Use the `--prod` flag for a production build. 

## Deployment

After build, the `sites` folder should be placed in back-end project in another repo at (https://gitlab.com/aalquaitiuni/teamic-backend-webapi)


## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).


## Reference

Angular DataTables: https://l-lin.github.io/angular-datatables/
