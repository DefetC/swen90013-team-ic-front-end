
import { NgModule } from '@angular/core';

import { SharedModule } from './shared/shared.module';
import { StudentModule } from './student/student.module';
import { AdminModule } from './admin/admin.module';
import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';

// import { ElModule } from 'element-angular';



@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    StudentModule,
    SharedModule,
    AdminModule,
    AppRoutingModule,
    // ElModule.forRoot(),
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule { }
