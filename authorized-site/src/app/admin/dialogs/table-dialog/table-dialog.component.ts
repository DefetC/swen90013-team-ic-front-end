import {Component, Inject, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Subject, Subscription} from 'rxjs';

import {DataService} from '../../data.service';

import {MAT_DIALOG_DATA, MatDialog, MatTableDataSource} from '@angular/material';
import {MatPaginator} from '@angular/material/paginator';

import {PdfDialogComponent} from '../pdf-dialog/pdf-dialog.component';

export interface File {
  student_id: number;
  studentName: string;
  filename: string;
  confidenceNumber: number;

}

@Component({
  selector: 'app-table-dialog',
  templateUrl: './table-dialog.component.html',
  styleUrls: ['./table-dialog.component.scss']
})


export class TableDialogComponent implements OnInit, OnDestroy {

  private bufferedFileListener = new Subject<Object>();
  private wordsListenerSub: Subscription;
  private algorithmListenerSub: Subscription;
  private dataStatusListenerSub: Subscription;


  featureWords = [];

  algorithmData = {};

  loading = true;
  average = 0;
  stats = [];

  @ViewChild(MatPaginator) paginator: MatPaginator;

  displayedColumns: string[] = ['student_id', 'filename', 'studentName', 'confidenceNumber'];
  dataSource = new MatTableDataSource([]);
  files: File[];
  // dataService: any;


  constructor(@Inject(MAT_DIALOG_DATA) public data: any, public dialog: MatDialog, private dataService: DataService ) { }
  ngOnInit() {
    this.files = this.data;
    this.dataSource = new MatTableDataSource(this.files);
    this.dataSource.paginator = this.paginator;
    this.loading = false;
    this.average = this.files[0].confidenceNumber;



    this.dataService.getWordsData();
    this.dataService.getAlgorithmData();

    // register loading subscription to let components know when data is loading
    this.dataStatusListenerSub = this.dataService.getDataStatusListener()
        .subscribe(val => {
          this.loading = val;
        });

    // register data subscription to let components get data once retrieved
    this.wordsListenerSub = this.dataService.getWordsDataListener()
        .subscribe(data => {
          // if no data is not available, fill it with 0s
          if (data == null) {
            data = {
              'featureWords' : ['test1', 'test2', 'test3', 'test4', 'test5'],
            };
          }

          // construct an array for displaying in the table
          this.featureWords = data['stats']['sentences'];
        });
    this.algorithmListenerSub = this.dataService.getAlgorithmDataListener()
        .subscribe(data => {
          if (data == null) {
            data = {};
          }
          this.algorithmData = data['stats'];
        });
  }

    applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  getRow(row) {
    console.log(row);
    this.dialog.open(PdfDialogComponent,
      {height: '500px', width: '700px', data: row.filename},
      );
  }

  closeDialog() {
    this.dialog.closeAll();
  }

  hasHistory() {
    if (this.featureWords.length <= 3) {
      return false;
    } else {
      return true;
    }
  }

  ngOnDestroy () {
    // unsubscribe all the subscription
    this.wordsListenerSub.unsubscribe();
    this.dataStatusListenerSub.unsubscribe();
  }
}
