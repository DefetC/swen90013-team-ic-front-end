import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Inject } from '@angular/core';

import { MAT_DIALOG_DATA } from '@angular/material';

import { UtilService } from '../../../shared/service/util.service';
import { FileDataModel } from '../../../shared/model/file-data.model';



@Component({
  selector: 'app-pdf-dialog',
  templateUrl: './pdf-dialog.component.html',
  styleUrls: ['./pdf-dialog.component.scss']
})
export class PdfDialogComponent implements OnInit {




  public file: FileDataModel;
  constructor(private http: HttpClient, private utilService: UtilService, @Inject(MAT_DIALOG_DATA) public data: any) { }

  totalPages: number;
  page: number;
  isLoaded: Boolean = false;


  nextPage() {
    if (this.page < this.totalPages) {
      this.page += 1;
    }
  }

  previousPage() {
    this.page -= 1;
  }

  afterLoadComplete(pdfData: any) {
    this.totalPages = pdfData.numPages;
    this.page = 1;
    this.isLoaded = true;
  }

  ngOnInit() {

    this.http.get(this.utilService.getBaseUrl() + '/file/student?filename=' + this.data, {responseType: 'blob'})
      .subscribe(file => {
        const reader = new FileReader();
        if (file.type === 'application/pdf') {
          reader.readAsArrayBuffer(file);
        } else if (file.type === 'text/plain') {
          reader.readAsText(file);
        }
        reader.onloadend = () => {
          console.log('test');
          this.file = {
              fileName: this.data,
              type: file.type,
              data: reader.result
          };
        };
      });
        }
}
