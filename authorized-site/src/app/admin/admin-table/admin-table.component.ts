// the imported packages and libraries
import { Component, OnDestroy, OnInit, ViewChild, Inject} from '@angular/core';
import { Subscription } from 'rxjs';
import { MatDialog, MatTableDataSource } from '@angular/material';
import { MatPaginator } from '@angular/material/paginator';
import { SelectionModel } from '@angular/cdk/collections';
import { MatSort } from '@angular/material/sort';
import { TableDialogComponent } from '../dialogs/table-dialog/table-dialog.component';
import { DataService } from '../data.service';

// Submission interface which mimics the model of a submission to handle the data processing.
export interface Submission {
  student_id: number;
  student_name: string;
  files: [];
  submission_id: string;
  validated: boolean;
}

// Component declarations
@Component({
  selector: 'app-admin-table',
  templateUrl: './admin-table.component.html',
  styleUrls: ['./admin-table.component.scss']
})

// Class declaration
export class AdminTableComponent implements OnInit, OnDestroy {
  showInstruction = false;
  showMsgTag = false;

  loading = true; // flag to manage loading animaiton
  message = '';  // message to be displayed in the case of possible errors

  // the indicators which represent the columns in the table view
  displayedColumns: string[] = ['student_id', 'student_name', 'submission_id', 'validated', 'rejected'];

  // data listener subscriptions
  private dataListenerSub: Subscription;
  private dataStatusListenerSub: Subscription;

  dataSource = new MatTableDataSource([]); // datasource to be filled to show data on material table
  submissions: Submission[]; // to store the submission objects in an array.

  // status variables that will be used for approving or rejecting a submission / submissions
  approvalStatus: String;
  rejectionStatus: String;

  // selection model elements
  private selection = new SelectionModel<Submission>(true, []); // for approvals
  private selection2 = new SelectionModel<Submission>(true, []); // for rejections

// Matsort and Matpaginator childs which works with material table to serve sorting and pagination
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  // constructor
  constructor(private dataService: DataService, public dialog: MatDialog) { }

  // the function for initiating the page
  ngOnInit() {
    // data status service connection initiation
    this.dataStatusListenerSub = this.dataService.getDataStatusListener()
      .subscribe(val => {
        this.loading = val;
      });

      // data listenener service connection initiation
    this.dataListenerSub = this.dataService.getTableDataListener()
      .subscribe((submissions: Submission[]) => {
        this.dataSource = new MatTableDataSource(submissions); // data table initiation
        this.dataSource.sort = this.sort;       // sorting initiation
        this.dataSource.paginator = this.paginator; // pagination initiation

        // The code portion below gets all the already approved submissions and sends it to the table.
        const alreadySelected = [];
        const alreadyRejected = [];

        // the loop for filling the table rows
        for (let i = 0; i < this.dataSource.data.length; i++) {
          if (this.dataSource.data[i].validated === true) {
            alreadySelected.push(this.dataSource.data[i]);
          } else if (this.dataSource.data[i].validated === false) {
            alreadyRejected.push(this.dataSource.data[i]);
          }
        }
        // filling the approval and rejection selection models
        this.selection = new SelectionModel<Submission>(true, alreadySelected);
        this.selection2 = new SelectionModel<Submission>(true, alreadyRejected);
      });
      this.dataStatusListenerSub = this.dataService.getDataStatusListener()
      .subscribe(val => {
        this.loading = val;
      });

    this.dataListenerSub = this.dataService.getApprovalListener()
      .subscribe((result: String) => {
        this.approvalStatus = result;
      });

    this.dataListenerSub = this.dataService.getRejectionListener()
      .subscribe((result: String) => {
        this.rejectionStatus = result;
      });

  }
  // this portion applies the filter which is performed by using the search box
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  // this function checks whether all the rows are selected as approved or not
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataSource.data.forEach(row => this.selection.select(row));
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: Submission): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.submission_id + 1}`;
  }

  // this function checks whether all the rows are selected as rejected or not
  isAllSelected2() {
    const numSelected = this.selection2.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle2() {
    this.isAllSelected2() ?
        this.selection2.clear() :
        this.dataSource.data.forEach(row => this.selection2.select(row));
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel2(row?: Submission): string {
    if (!row) {
      return `${this.isAllSelected2() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection2.isSelected(row) ? 'deselect' : 'select'} row ${row.submission_id + 1}`;
  }

  // The function changes the status of a row from approved to rejected when the user clicks on reject checkbox
  changeSelectionApproveToReject(row) {
    const alreadyApproved = [];
    for (let i = 0; i < this.selection.selected.length; i++) {
      if (this.selection.selected[i].submission_id !== row.submission_id) {
        alreadyApproved.push(this.selection.selected[i]);
      }
    }
    this.selection = new SelectionModel<Submission>(true, alreadyApproved);
  }

  // The function changes the status of a row from rejected to approved when the user clicks on approve checkbox
  changeSelectionRejectToApprove(row) {
    const alreadyRejected = [];
    for (let i = 0; i < this.selection2.selected.length; i++) {
      if (this.selection2.selected[i].submission_id !== row.submission_id) {
        alreadyRejected.push(this.selection2.selected[i]);
      }
    }
    this.selection2 = new SelectionModel<Submission>(true, alreadyRejected);
  }

  // this function changes the status of each row as approved when select all (approve) is clicked by the user
  changeSelectionAllToApprove() {
    this.selection2 = new SelectionModel<Submission>(true, []);
  }

  // this function changes the status of each row as rejected when select all (reject) is clicked by the user
  changeSelectionAllToReject() {
    this.selection = new SelectionModel<Submission>(true, []);
  }

  // destorying the page
  ngOnDestroy () {
    this.dataListenerSub.unsubscribe();
    this.dataStatusListenerSub.unsubscribe();
  }

  // this function opens the dialog for showing the submitted files for a particular submission
  openDialog(row): void {
    const dataTemp = [];

    for (let i = 0; i < this.dataSource.data.length; i++) {
      if (row.submission_id === this.dataSource.data[i].submission_id) {
        for (let j = 0; j < this.dataSource.data[i].files.length; j++) {
          dataTemp.push({
            student_name: this.dataSource.data[i].student_name,
            student_id: this.dataSource.data[i].student_id,
            filename: this.dataSource.data[i].files[j].filename,
            confidenceNumber: Math.round(this.dataSource.data[i].files[j].confidenceNumber * 1000) / 1000
          });
        }
      }
    }
    this.dialog.open(TableDialogComponent,
      {data: dataTemp, height: '500px', width: '900px'},
      );
  }

  // this function returns all the selected rows.
  getSelections() {
    const approvals = [];
    const rejections = [];
    for (let i = 0; i < this.selection.selected.length; i++) {
      approvals.push(this.selection.selected[i].submission_id);
    }

    for (let i = 0; i < this.selection2.selected.length; i++) {
      rejections.push(this.selection2.selected[i].submission_id);
    }

    this.dataService.sendApprovalList(approvals);
    this.dataService.sendRejectionList(rejections);

    this.showMsgTag = true;
  }

  getShowInstruction() {
    this.showInstruction = !this.showInstruction;
  }
}
