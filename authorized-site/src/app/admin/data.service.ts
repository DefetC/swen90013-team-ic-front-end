import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Subject } from 'rxjs';


import { UtilService } from '../shared/service/util.service';

@Injectable({
    providedIn: 'root'
})
export class DataService {
    private chartDataListener = new Subject<Object>();
    private tableDataListener = new Subject<Object>();
    private dataStatusListener = new Subject<boolean>();
    private dataApprovalListener = new Subject<String>();
    private dataRejectionListener = new Subject<String>();

    private wordsDataListener = new Subject<Object>();
    private algorithmDataListener = new Subject<Object>();


    constructor(private http: HttpClient, private utilService: UtilService) { }


    // method calls to let components get listener from the service
    // which gives them the ability to listen to specific incoming data.
    getChartDataListener() {
        return this.chartDataListener.asObservable();
    }

    getTableDataListener() {
        return this.tableDataListener.asObservable();
    }

    getDataStatusListener() {
        return this.dataStatusListener.asObservable();
    }

    getApprovalListener() {
        return this.dataApprovalListener.asObservable();
    }

    getRejectionListener() {
        return this.dataRejectionListener.asObservable();
    }

    getWordsDataListener() {
        return this.wordsDataListener.asObservable();
    }

    getAlgorithmDataListener() {
        return this.algorithmDataListener.asObservable()
    }

    // the incoming method call used to get chart data based on input
    getChartData(workshopId) {
        // let the components know we starting loading chart data
        this.dataStatusListener.next(true);

        // if workshopId is undefined, it means it is instructor site
        // we get chart data regarding to the whole subject
        // otherwise it is tutor site, we get chart data regarding to the workshop
        if (workshopId === undefined) {
            this.getInstructorChartData();
        } else {
            this.getTutorChartData(workshopId);
        }
    }


    // private method to help retrieve chart data of specific sections/workshop of specific subject
    private getTutorChartData(workshopId) {
        this.http.get(this.utilService.getBaseUrl() + '/stats/workshop/' + workshopId
        + '/assignment/' + this.utilService.getAssignmentId())
            .subscribe(response => {
                // pass retrieved data to the components
                this.chartDataListener.next(response);

                // let the components know loading chart data is finished
                this.dataStatusListener.next(false);
            });
    }

    // private method to help retrieve chart data of specific subject
    private getInstructorChartData() {
        this.http.get(this.utilService.getBaseUrl() + '/stats/subject/' + this.utilService.getCourseId()
        + '/assignment/' + this.utilService.getAssignmentId())
        // this.http.get(this.utilService.getDynamicUrl('/stats/subject/' + this.utilService.getCourseId()), { withCredentials: true })
            .subscribe(response => {
                // pass retrieved data to the components
                this.chartDataListener.next(response);

                // let the components know loading chart data is finished
                this.dataStatusListener.next(false);
            });
    }

    // the incoming method call used to get table data based on input
    getTableData(workshopId) {
        // let the components know we starting loading chart data
        this.dataStatusListener.next(true);

        // if workshopId is undefined, it means it is instructor site
        // we get table data regarding to the whole subject
        // otherwise it is tutor site, we get table data regarding to the workshop
        if (workshopId === undefined) {
            this.getInstructorTableData();
        } else {
            this.getTutorTableData(workshopId);
        }
    }

    // private method to help retrieve table data of specific sections/workshop of specific subject
    private getTutorTableData(workshopId) {
        const url = this.utilService.getBaseUrl() + '/submissions?workshop_id='
            + workshopId + '&assignment_id=' + this.utilService.getAssignmentId();
        this.http.get(url)
        .subscribe(response => {
            // pass retrieved data to the components
            this.tableDataListener.next(response['data']);

            // let the components know loading table data is finished
            this.dataStatusListener.next(false);
        });
    }

    // private method to help retrieve table data of specific subject
    private getInstructorTableData() {
        const url = this.utilService.getBaseUrl() + '/submissions?subject_id=' + this.utilService.getCourseId()
        + '&assignment_id=' + this.utilService.getAssignmentId();
        this.http.get(url)
        .subscribe(response => {
            // pass retrieved data to the components
            this.tableDataListener.next(response['data']);

            // let the components know loading table data is finished
            this.dataStatusListener.next(false);
        });
    }


    sendApprovalList(selection) {
        const body = {'submission_id': selection};
        this.http
            .post(this.utilService.getBaseUrl() + '/submissions/approve', body, {headers: {'Content-Type': 'application/json'}})
            .subscribe(response => {
                this.dataStatusListener.next(false);
                this.dataApprovalListener.next(response['message']);
            });

    }

    sendRejectionList(selection) {

        const body = {'submission_id': selection};

        this.http
            .post(this.utilService.getBaseUrl() + '/submissions/reject', body, {headers: {'Content-Type': 'application/json'}})
            .subscribe(response => {
                this.dataStatusListener.next(false);
                this.dataApprovalListener.next(response['message']);
            });
    }

    // get the similar word methods
    getWordsData() {
        const url = this.utilService.getBaseUrl() + '/words/subject/' + this.utilService.getCourseId()
                    + '/assignment/' + this.utilService.getAssignmentId();
        this.http.get(url).subscribe(response => {
            this.wordsDataListener.next(response);
            this.dataStatusListener.next(false);
        });
    }

    // get the algorithms data
    getAlgorithmData() {
        const url = this.utilService.getBaseUrl() + '/algorithm/subject/' + this.utilService.getCourseId()
            + '/assignment/' + this.utilService.getAssignmentId();
        this.http.get(url).subscribe(response => {
            this.algorithmDataListener.next(response);
            this.dataStatusListener.next(false);
        });
    }
}
