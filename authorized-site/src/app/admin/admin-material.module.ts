
import { NgModule } from '@angular/core';

import { MatDialogModule,  MatInputModule, MatCheckboxModule, MatFormFieldModule,
  MatTableModule, MatPaginatorModule, MatSortModule, MatDividerModule, MatListModule, MatCardModule } from '@angular/material';

import { ScrollingModule } from '@angular/cdk/scrolling';

/**
 * This Module include every material component used related to the instructor and tutor sits
 * If you want to update on material component instructor or tutor site
 * You should update under the /admin folder and add/remove material components and module in this file
 */

@NgModule({
  declarations: [
  ],
  imports: [
    MatDialogModule,
    MatInputModule,
    MatCheckboxModule,
    MatFormFieldModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatDividerModule,
    MatListModule,
    MatCardModule,
    ScrollingModule
  ],
  exports: [
    MatDialogModule,
    MatInputModule,
    MatCheckboxModule,
    MatFormFieldModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatDividerModule,
    MatListModule,
    MatCardModule,
    ScrollingModule
  ],
  providers: [],
  bootstrap: [],
})
export class AdminMaterialModule { }
