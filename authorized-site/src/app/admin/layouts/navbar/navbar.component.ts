import { Component, OnInit } from '@angular/core';

import { SidebarService } from '../sidebar/sidebar.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent {

  constructor(private sidebarService: SidebarService) { }

  // once click on different types
  // update showing data based on new selected type
  updateDisplayType(type) {
    this.sidebarService.updateDisplayType(type);
  }

}
