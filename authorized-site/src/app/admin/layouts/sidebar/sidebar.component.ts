import { Component, Input } from '@angular/core';
import { WorkshopModel } from '../../../shared/model/workshopModel';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent {
  @Input() workshops: WorkshopModel[];
  constructor() { }
}
