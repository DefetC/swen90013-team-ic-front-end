import { Component, Input } from '@angular/core';
import { SidebarService } from '../sidebar.service';
import { WorkshopModel } from 'src/app/shared/model/workshopModel';

@Component({
  selector: 'app-sidebar-section',
  templateUrl: './sidebar-section.component.html',
  styleUrls: ['./sidebar-section.component.scss']
})
export class SidebarSectionComponent {
  @Input() workshop: WorkshopModel;
  constructor(private sidebarService: SidebarService) { }

  // when user select this workshop, update data based on current workshop id
  select() {
    this.sidebarService.selectWorkshop(this.workshop.workshopId);
  }

}
