import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Subject } from 'rxjs';


import { WorkshopModel } from 'src/app/shared/model/workshopModel';
import { AuthService } from 'src/app/shared/service/auth.service';
import { UtilService } from 'src/app/shared/service/util.service';
import { DataService } from '../../data.service';

@Injectable({
    providedIn: 'root'
})
export class SidebarService {

    private availableWorkshopsListener = new Subject<WorkshopModel[]>();
    private availableWorkshopsStatusListener = new Subject<boolean>();

    // array of all workshops and their corresponding ids
    private workshops: WorkshopModel[];

    // initial data show type is chart view
    private displayType = 'Chart';

    // variable to save current select workshop id
    private selectedWorkshopId;

    // inject all needed service into this service
    constructor(private http: HttpClient, private utilService: UtilService,
                private authService: AuthService,
                private dataService: DataService) { }

    // method calls to let components get listener from the service
    // which gives them the ability to listen to specific incoming data.
    getAvailableWorkshopsListener() {
        return this.availableWorkshopsListener.asObservable();
    }

    getAvailableWorkshopsStatusListener() {
        return this.availableWorkshopsStatusListener.asObservable();
    }


    // update display type based on input 'Chart' and 'Table'
    updateDisplayType(type) {
        // if type has been changed, do the work
        // otherwise do nothing.
        if (type !== this.displayType) {
            // update display type
            this.displayType = type;

            // get correct data based on user's enrollment type in the course
            if (this.authService.getUserDetails()['enrollmentType'] === 'TeacherEnrollment') {
                if (this.displayType === 'Chart') {
                    this.dataService.getChartData(undefined);
                } else if (this.displayType === 'Table') {
                    this.dataService.getTableData(undefined);
                }
            } else {
                if (this.selectedWorkshopId === undefined) {
                    this.selectWorkshop(this.workshops[0].workshopId);
                    this.selectedWorkshopId = this.workshops[0].workshopId;
                } else {
                    this.selectWorkshop(this.selectedWorkshopId);
                }
            }
        }
    }

    // get all workshops related to this tutor of current course
    getAvailableWorkshops() {
        // let the components know we starting get all available workshops
        this.availableWorkshopsStatusListener.next(true);

        this.http.get(this.utilService.getBaseUrl() + '/display/workshop/'
            + this.utilService.getCourseId() + '/' + this.authService.getUserID())
            .subscribe(response => {
                // construct workshop model based on incoming data
                const new_workshops = [];
                response['sections'].forEach(element => {
                    new_workshops.push({
                        workshopId: element['id'],
                        workshopName: element['name'],
                        selected: false
                    });
                });
                // save newly created workshops
                this.workshops = new_workshops;

                // send newly created workshops to sidebar to update views
                this.availableWorkshopsListener.next(this.workshops);

                // once get the all the subject, we retrieve data of the first subject
                if (this.workshops.length > 0) {
                    this.selectWorkshop(this.workshops[0]['workshopId']);
                }

                // let the components know all available workshops is successfully loaded
                this.availableWorkshopsStatusListener.next(false);
            });
    }

    // select a certain subject
    selectWorkshop(workshopId) {
        // select current selected workshop id to user selected workshop id
        this.selectedWorkshopId = workshopId;
        // update workshop model which aims to update views on sidebar
        this.workshops.forEach(function(el, index, arr) {
            if (arr[index]['workshopId'] === workshopId) {
                arr[index]['selected'] = true;
            } else {
                arr[index]['selected'] = false;
            }
        });

        // let sidebar update its view based on selected workshop
        this.availableWorkshopsListener.next(this.workshops);

        // start retrieving data based on current showing type
        if (this.displayType === 'Chart') {
            this.dataService.getChartData(workshopId);
        } else if (this.displayType === 'Table') {
            this.dataService.getTableData(workshopId);
        }
    }

}
