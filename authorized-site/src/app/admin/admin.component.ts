import { Component, Input } from '@angular/core';

import { WorkshopModel } from '../shared/model/workshopModel';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent {
  loading;
  @Input() userDetails;
  @Input() workshops: WorkshopModel[];
  constructor()  {}
}
