import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { PdfViewerModule } from 'ng2-pdf-viewer';

import { AdminMaterialModule } from './admin-material.module';
import { SharedModule } from '../shared/shared.module';

import { AdminComponent } from './admin.component';
import { NavbarComponent } from './layouts/navbar/navbar.component';
import { SidebarComponent } from './layouts/sidebar/sidebar.component';
import { SidebarSectionComponent } from './layouts/sidebar/sidebar-section/sidebar-section.component';
import { AdminTableComponent } from './admin-table/admin-table.component';
import { ChartViewComponent } from './chart-view/chart-view.component';
import { ChartViewRingComponent } from './chart-view/chart-view-ring/chart-view-ring.component';
import { TableDialogComponent } from './dialogs/table-dialog/table-dialog.component';
import { PdfDialogComponent } from './dialogs/pdf-dialog/pdf-dialog.component';

import { DataService } from './data.service';

/**
 * This Module include everything related to the instructor and tutor sits
 * If you want to update on instructor or tutor site
 * You should update under the /admin folder and add/remove components and module in this file
 */
@NgModule({
  imports: [CommonModule, RouterModule, PdfViewerModule, AdminMaterialModule, SharedModule],
  declarations: [
    AdminTableComponent,
    NavbarComponent,
    SidebarComponent,
    SidebarSectionComponent,
    ChartViewComponent,
    ChartViewRingComponent,
    TableDialogComponent,
    AdminComponent,
    PdfDialogComponent],
  exports: [AdminComponent],
  entryComponents: [AdminComponent, TableDialogComponent, PdfDialogComponent],
  providers: [DataService]
})
export class AdminModule {}
