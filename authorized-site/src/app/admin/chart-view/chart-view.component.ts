import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

import { DataService } from '../data.service';


@Component({
  selector: 'app-chart-view',
  templateUrl: './chart-view.component.html',
  styleUrls: ['./chart-view.component.scss']
})
export class ChartViewComponent  implements OnInit, OnDestroy {

  loading = true;
  average = 0;
  stats = {};
  private dataListenerSub: Subscription;
  private dataStatusListenerSub: Subscription;
  showInstruction = false;

  constructor(private dataService: DataService) { }

  ngOnInit() {

    // register loading subscription to let components know when data is loading
    this.dataStatusListenerSub = this.dataService.getDataStatusListener()
      .subscribe(val => {
        this.loading = val;
      });

    // register data subscription to let components get data once retrieved
    this.dataListenerSub = this.dataService.getChartDataListener()
      .subscribe(data => {
        // if no data is not available, fill it with 0s
        if (data == null) {
          data =  {
            'stats': {
                'highest': 0,
                'lowest': 0,
                'average': 0,
                'median': 0,
                'totalNum': 0,
            }
          };
        }

        // construct an array for displaying in the table
        this.stats = {};
        this.stats = data['stats'];
        this.average = data['stats']['average'];
        for (const key in this.stats) {
            if (key !== 'totalNum') {
                this.stats[key] = (this.stats[key] * 100).toFixed(2 ) + '%';
            }
        }

        // // go thru all type of data
        // for (const key in data['stats']) {
        //   if (key === 'average') {
        //     // average score should be saved in an extra field to be used for rings
        //     this.average = data['stats'][key];
        //   } else  if (key === 'totalNum') {
        //       // save data in array and rename
        //       this.stats.push({
        //           dataType: 'count',
        //           value: data['stats'][key]
        //       });
        //   } else {
        //     // save data in array
        //     this.stats.push({
        //       dataType: key,
        //       value:  (data['stats'][key] * 100) + '%'
        //     });
        //   }
        // }
      });

  }

  getShowInstruction() {
      this.showInstruction = !this.showInstruction;
  }

  ngOnDestroy () {
    // unsubscribe all the subscription
    this.dataListenerSub.unsubscribe();
    this.dataStatusListenerSub.unsubscribe();
  }
}
