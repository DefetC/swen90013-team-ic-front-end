import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChartViewRingComponent } from './chart-view-ring.component';

describe('ChartViewRingComponent', () => {
  let component: ChartViewRingComponent;
  let fixture: ComponentFixture<ChartViewRingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChartViewRingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChartViewRingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
