import { Component, OnInit, Input, AfterViewInit } from '@angular/core';
import * as d3 from 'd3';

import { DataRingConfiguration } from 'src/app/shared/model/data-ring-configuration';



@Component({
  selector: 'app-chart-view-ring',
  templateUrl: './chart-view-ring.component.html',
  styleUrls: ['./chart-view-ring.component.scss']
})
export class ChartViewRingComponent implements OnInit, AfterViewInit {
  // configuration for the ring using d3
  private configs: DataRingConfiguration[] = [
    {
      dataType: 'average',
      dimension: 400,
      description: 'Average Confidence'
    }
  ];

  // input from 'chart-view' component
  @Input() average;

  // all following variable and methods are  related to d3
  // if you need to change the ring
  // please refer to d3 official documents
  private foreground = [];
  private percentComplete = [];
  private arc = [];
  private svg = [];
  private formatPercent = d3.format('.0%');


  constructor() { }

  private initializeSVG() {

    // initial four rings
    for (let i = 0; i < this.configs.length; i++ ) {

      this.svg.push(d3.select('.ring-' + i).append('svg')
      .attr('width', this.configs[i].dimension)
      .attr('height', this.configs[i].dimension)
      .append('g')
      .attr('transform', 'translate(' + this.configs[i].dimension / 2 + ',' + this.configs[i].dimension / 2 + ')'));

      this.arc.push(d3.arc()
      .startAngle(0)
      .innerRadius(this.configs[i].dimension / (i === 0 ? 3 : 2.5))
      .outerRadius(this.configs[i].dimension / (i === 0 ? 3.5 : 3)));

      const meter = this.svg[i].append('g')
          .attr('class', 'funds-allocated-meter');

      meter.append('path')
          .attr('class', 'background')
          .attr('d', this.arc[i].endAngle(2 * Math.PI))
          .attr('fill', '#E6E7E8');

      this.foreground.push(meter.append('path')
          .attr('class', 'foreground'));

      this.percentComplete.push(meter.append('text')
          .attr('text-anchor', 'middle')
          .attr('class', 'percent-complete')
          .attr('dy', '0em')
          .attr('font-family', '\'Roboto Condensed\', sans-serif')
          .attr('font-size', this.configs[i].dimension / 10 +  'px')
          .attr('fill', '#5B8BE3')
          .text('0%'));

      const description = meter.append('text')
          .attr('text-anchor', 'middle')
          .attr('class', 'description')
          .attr('dy', '2.3em')
          .attr('font-size', (this.configs[i].dimension / (i === 0 ? 20 : 16)))
          .attr('fill', '#7E7E7E')
          .text(this.configs[i].description);
    }

  }

  ngAfterViewInit() {
    this.initializeSVG();
  }

  ngOnInit() {
    const progress = [];
    const score = [];
    const i = [];
    for (let index = 0; index < this.configs.length; index++) {
      progress.push(0);
      score.push(this.average);
      i.push(d3.interpolate(progress[index], score[index]));
    }
    // total time for animation
    const that = this;
    d3.transition().duration(1500).tween('progress', function() {
      return function(t) {
        for (let index = 0; index < that.configs.length; index++) {
          progress[index] = i[index](t);
          let color = '#F15642';
          if (progress[index] >= 0.83) {
            color = '#70AD47';
          } else if (progress[index] >= 0.4) {
            color = '#F3A712';
          }
          that.foreground[index].attr('d', that.arc[index].endAngle(2 * Math.PI * progress[index])).attr('fill', color);
          that.percentComplete[index].text(that.formatPercent(progress[index]));
        }
      };
    });
  }
}
