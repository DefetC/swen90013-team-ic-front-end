import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PreviewComponent } from './student/preview/preview.component';
import { UploadGuard } from './student/student.guard';
import { UploadSuccessComponent } from './student/upload-success/upload-success.component';
import { MultiUploadComponent } from './student/multi-upload/multi-upload.component';
import { UploadHomeComponent } from './student/upload-home/upload-home.component';
import { StudentGuard } from './shared/student.guard';
import { TeacherGuard } from './shared/teacher.guard';
import { AdminTableComponent } from './admin/admin-table/admin-table.component';
import { ChartViewComponent } from './admin/chart-view/chart-view.component';
import { PreviousSubmissionComponent } from './student/previous-submission/previous-submission.component';


const routes: Routes = [
  {
    path: 'upload',
    children: [
      {path: 'files', component: MultiUploadComponent},
      {path: 'preview', component: PreviewComponent, canActivate: [UploadGuard]},
      {path: 'previous', component: PreviousSubmissionComponent},
      {path: 'success', component: UploadSuccessComponent, canActivate: [UploadGuard]},
      {path: '', component: UploadHomeComponent},
      {path: '**', redirectTo: 'upload', pathMatch: 'full'},
      {path: '', redirectTo: 'upload', pathMatch: 'full'},
    ],
    canActivate: [StudentGuard],
  },
  {
    path: 'admin',
    // component: ChartViewComponent,
    children: [
      {path: 'table', component: AdminTableComponent},
      {path: 'chart', component: ChartViewComponent},
      {path: '', component: ChartViewComponent},
      {path: '**', redirectTo: 'chart', pathMatch: 'full'},
      {path: '', redirectTo: 'chart', pathMatch: 'full'},
    ],
    canActivate: [TeacherGuard],
  },
  {path: '**', redirectTo: 'upload', pathMatch: 'full'},
  {path: '', redirectTo: 'upload', pathMatch: 'full'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
