import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

import { UploadService } from '../upload.service';

@Component({
  selector: 'app-upload-home',
  templateUrl: './upload-home.component.html',
  styleUrls: ['./upload-home.component.scss']
})
export class UploadHomeComponent implements OnInit, OnDestroy {
  hasPrevListenerSub: Subscription;
  loadingListenerSub: Subscription;
  loading = false;
  hasPrev = false;
  constructor(private uploadService: UploadService) { }
  // start new submission
  startNewSubmission() {
    this.uploadService.startNewSubmission();
  }

  // check previous submission
  checkPreviousWork() {
    this.uploadService.checkPreviousWork();
  }

  ngOnInit() {
    this.hasPrevListenerSub = this.uploadService.getHasPrevListenerSub()
      .subscribe(hasPrev => {
        this.hasPrev = hasPrev;
        this.loading = false;
      });
    this.loadingListenerSub = this.uploadService.getLoadingListener()
      .subscribe(loading => {
        this.loading = loading;
      });
    this.uploadService.checkPrev();
  }

  ngOnDestroy() {
    this.hasPrevListenerSub.unsubscribe();
    this.loadingListenerSub.unsubscribe();
  }

}
