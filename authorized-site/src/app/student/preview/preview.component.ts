import { Component, OnInit } from '@angular/core';

import { MatDialog } from '@angular/material';

import { UploadService } from '../upload.service';
import { UploadDialogComponent } from '../upload-dialog/upload-dialog.component';



@Component({
  selector: 'app-preview',
  templateUrl: './preview.component.html',
  styleUrls: ['./preview.component.scss']
})
export class PreviewComponent implements OnInit {
  bufferedFiles = [];
  selectedIndex = 0;
  Math: Math;
  constructor(private uploadService: UploadService, public dialog: MatDialog) {this.Math = Math; }

  totalPages: number;
  page: number;
  isLoaded: Boolean = false;


  nextPage() {
    if (this.page < this.totalPages) {
      this.page += 1;
    }
  }

  previousPage() {
    this.page -= 1;
  }

  afterLoadComplete(pdfData: any) {
    this.totalPages = pdfData.numPages;
    this.page = 1;
    this.isLoaded = true;
  }

  ngOnInit() {
    this.bufferedFiles = this.uploadService.getBufferedFiles();
  }

  // update select file index to update file to display
  select(index) {
    this.selectedIndex = index;
  }

  // when user want to go back, prompt to user
  // whether to go back
  goBack () {
    const dialogRef = this.dialog.open(UploadDialogComponent, {
      width: '250px',
      data: 'Upload'
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

  // submit current uploaded files
  submit() {
    this.uploadService.submitAssignment();
  }

}
