import { Component, Inject } from '@angular/core';

import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { UploadService } from '../upload.service';

@Component({
  selector: 'app-upload-dialog',
  templateUrl: './upload-dialog.component.html',
  styleUrls: ['./upload-dialog.component.scss']
})
export class UploadDialogComponent {

  constructor(
    public dialogRef: MatDialogRef<UploadDialogComponent>,
    private uploadService: UploadService,
    @Inject(MAT_DIALOG_DATA) public data: String) {}

  // if user select not to go back, do nothing
  onNoClick(): void {
    this.dialogRef.close();
  }

  // if user select to go back, clear all selected/cached files
  onOkClick(): void {
    this.dialogRef.close();
    if (this.data === 'Home') {
      this.uploadService.goBackHome();
    } else {
      this.uploadService.cancelSubmission();
    }
  }
}
