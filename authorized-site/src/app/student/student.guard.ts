import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { UploadService } from './upload.service';

@Injectable({providedIn: 'root'})
export class UploadGuard implements CanActivate {
    constructor(private uploadService: UploadService, private router: Router) { }

    canActivate(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ): boolean | Observable<boolean> | Promise<boolean>  {
        const isUploaded = this.uploadService.getIsUploadedSuccessfully();
        console.log(isUploaded);
        if (!isUploaded) {
            this.router.navigate(['upload/']);
        }
        return isUploaded;
    }
}


