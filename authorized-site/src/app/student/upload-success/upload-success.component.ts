import { Component, OnInit } from '@angular/core';

import { UploadService } from '../upload.service';

@Component({
  selector: 'app-upload-success',
  templateUrl: './upload-success.component.html',
  styleUrls: ['./upload-success.component.scss']
})
export class UploadSuccessComponent implements OnInit {

  submissionID;

  constructor(private uploadService: UploadService) { }

  ngOnInit() {
    this.submissionID  = this.uploadService.getSubmissionId();
  }

  goBackHome() {
    this.uploadService.goBackHome();
  }

}
