
import { NgModule } from '@angular/core';


import { MatDialogModule,  MatInputModule, MatCardModule, MatDividerModule,
  MatListModule, MatButtonModule, MatSnackBarModule, MatProgressSpinnerModule, MatProgressBarModule } from '@angular/material';

import { ScrollingModule } from '@angular/cdk/scrolling';

/**
 * This Module include every material component used related to the student sits
 * If you want to update on material component student site
 * You should update under the /student folder and add/remove material components and module in this file
 */


@NgModule({
  declarations: [
  ],
  imports: [
    MatDialogModule,
    MatInputModule,
    MatDividerModule,
    MatCardModule,
    MatListModule,
    MatButtonModule,
    MatSnackBarModule,
    MatProgressSpinnerModule,
    MatProgressBarModule,
    ScrollingModule
  ],
  exports: [
    MatDialogModule,
    MatInputModule,
    MatDividerModule,
    MatCardModule,
    MatListModule,
    MatButtonModule,
    MatSnackBarModule,
    MatProgressSpinnerModule,
    MatProgressBarModule,
    ScrollingModule
  ],
  providers: [],
  bootstrap: [],
})
export class StudentMaterialModule { }
