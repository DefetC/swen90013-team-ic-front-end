import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PreviousSubmissionComponent } from './previous-submission.component';

describe('PreviousSubmissionComponent', () => {
  let component: PreviousSubmissionComponent;
  let fixture: ComponentFixture<PreviousSubmissionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PreviousSubmissionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PreviousSubmissionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
