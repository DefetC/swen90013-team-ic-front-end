import { Component, OnInit, OnDestroy } from '@angular/core';
import { MatDialog } from '@angular/material';
import { Subscription } from 'rxjs';

import { UploadService } from '../upload.service';
import { FileDataModel } from 'src/app/shared/model/file-data.model';

@Component({
  selector: 'app-previous-submission',
  templateUrl: './previous-submission.component.html',
  styleUrls: ['./previous-submission.component.scss']
})
export class PreviousSubmissionComponent implements OnInit, OnDestroy {

  loading = false;
  // a list of object including type and data
  bufferedFiles: FileDataModel[] = [];
  math: Math;
  selectedIndex = -1;
  loadingListenerSub: Subscription;
  bufferedFileListenerSub: Subscription;
  getFileListListenerSub: Subscription;
  constructor(private uploadService: UploadService, public dialog: MatDialog) {this.math = Math; }

  totalPages: number;
  page: number;
  isLoaded: Boolean = false;


  nextPage() {
    if (this.page < this.totalPages) {
      this.page += 1;
    }
  }

  previousPage() {
    this.page -= 1;
  }

  afterLoadComplete(pdfData: any) {
    this.totalPages = pdfData.numPages;
    this.page = 1;
    this.isLoaded = true;
  }

  ngOnInit() {
    this.selectedIndex = -1;
    this.loadingListenerSub = this.uploadService.getLoadingListener()
      .subscribe(loading => {
      this.loading = loading;
    });
    this.getFileListListenerSub = this.uploadService.getFileListListener()
      .subscribe(files => {
        // file should be at least one;
        this.bufferedFiles = files;
        console.log(this.bufferedFiles);
        this.select(0);
      });

    this.bufferedFileListenerSub = this.uploadService.getBufferedFileListener()
      .subscribe(result => {
        // tslint:disable-next-line:radix
        const i = parseInt(result['index']);
        console.log(result);
        console.log(this.bufferedFiles);
        this.bufferedFiles[i] = {
          fileName: this.bufferedFiles[i]['fileName'],
          type: result['type'],
          data:  result['data']
        };
      });
    // get all the files from pervious submission
    this.uploadService.getPreviousSubmission();
  }

  ngOnDestroy() {
    this.loadingListenerSub.unsubscribe();
    this.getFileListListenerSub.unsubscribe();
    this.bufferedFileListenerSub.unsubscribe();
  }

  select(index) {
    if (this.selectedIndex === index) {
      return;
    }
    this.selectedIndex = index;
    // check if already loaded
    // if so, do nothing
    if (this.bufferedFiles[this.selectedIndex]['data'] !== null) {
      return;
    }
    // otherwise load the file
    this.uploadService.getBufferedFile(this.selectedIndex, this.bufferedFiles[this.selectedIndex]['fileName']);
  }

  goBack () {
    this.uploadService.goBackHome();
  }

}
