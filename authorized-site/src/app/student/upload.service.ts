import { Injectable } from '@angular/core';

import { Subject, Observable } from 'rxjs';
import { HttpClient, HttpHeaders, HttpRequest, HttpEventType } from '@angular/common/http';
import { Router } from '@angular/router';
import { AuthService } from '../shared/service/auth.service';
import { UtilService } from '../shared/service/util.service';
import { FileDataModel } from '../shared/model/file-data.model';

@Injectable({
    providedIn: 'root'
})
export class UploadService {

    bufferedFiles: FileDataModel[] = [];

    comments: String = '';

    uploading = false;

    file = null;

    AWSResponse = null;

    uploaded = false;

    submissionId;

    public files: File[] = [];

    public uploadedFiles: String[] = [];

    private filesListener = new Subject<File[]>();
    private hasPrevListener = new Subject<boolean>();
    private loadingListener = new Subject<boolean>();
    private fileListListener = new Subject<FileDataModel[]>();
    private bufferedFileListener = new Subject<Object>();

    constructor(private http: HttpClient, private router: Router, private authService: AuthService, private utilService: UtilService) { }

    getFileListener() {
        return this.filesListener.asObservable();
    }

    getLoadingListener() {
        return this.loadingListener.asObservable();
    }

    getBufferedFileListener() {
        return this.bufferedFileListener.asObservable();
    }

    getFileListListener() {
        return this.fileListListener.asObservable();
    }


    getSubmissionId() {
        return this.submissionId;
    }

    startNewSubmission() {
        this.router.navigate(['upload/files']);
    }

    getHasPrevListenerSub() {
        return this.hasPrevListener.asObservable();
    }

    // add selected to files pending to upload
    addFiles(newFiles) {
        const invalid = [false, false];
        for (const key in newFiles) {
            // tslint:disable-next-line:radix
            if (!isNaN(parseInt(key))) {
                const file = newFiles[key];
                // only support pdf and txt file
                if (file.type === 'application/pdf' || file.type === 'text/plain') {
                    // support up to 5MB files
                    if (file.size <= 5000000) {
                        this.files.push(file);
                    } else {
                        invalid[1] = true;
                    }
                } else {
                    invalid[0] = true;
                    if (file.size > 5000000) {
                        invalid[1] = true;
                        break;
                    }
                }
            }
        }
        this.filesListener.next(this.files);
        return invalid;
    }

    // remove added files
    removeFile(index) {
        this.files.splice(index, 1);
        this.filesListener.next(this.files);
    }

    getFiles() {
        return this.files;
    }

    // upload selected file to the server
    public upload(
        files: File[],
        comments: String,
    ): { [key: string]: { progress: Observable<number> } } {
        // save comments
        this.comments = comments;

        const status: { [key: string]: { progress: Observable<number> } } = {};

        // construct buffered files array
        for (let i = 0; i < files.length; i++) {
            this.bufferedFiles.push(null);
        }

        // buffer all selected file
        for (let i = 0; i < files.length; i++) {
            const reader = new FileReader();
            const file = files[i];
            if (file.type === 'application/pdf') {
                reader.readAsArrayBuffer(file);
            } else if (file.type === 'text/plain') {
                reader.readAsText(file);
            }
            reader.onloadend = () => {
                this.bufferedFiles[i] = {
                    type: file.type,
                    fileName: file.name,
                    data : reader.result,
                };
            };
        }

        // upload files to the server
        const url = this.utilService.getBaseUrl() + '/files';
        files.forEach(file => {
            // create a new multipart-form for every file
            const formData: FormData = new FormData();
            // replace any occurrence of ';' to '-' due to usage of ';' in algorithm for splitting
            formData.append('file', file, file.name.replace(/;/g, '-'));
            formData.append('student_id', this.authService.getUserID());
            formData.append('assignment_id', this.utilService.getAssignmentId());
            // create a http-post request and pass the form
            // tell it to report the upload progress
            const req = new HttpRequest('POST', url, formData, {
                reportProgress: true
            });
            // create a new progress-subject for every file
            const progress = new Subject<number>();

            // send the http-request and subscribe for progress-updates
            this.http.request(req).subscribe(event => {
                if (event.type === HttpEventType.UploadProgress) {
                // calculate the progress percentage
                    const percentDone = Math.round((100 * event.loaded) / event.total);
                    // pass the percentage into the progress-stream
                    progress.next(percentDone);
                } else if (event.type === HttpEventType.Response) {
                    // Close the progress-stream if we get an answer form the API
                    // The upload is complete
                    this.uploadedFiles.push(event.body['filename']);
                    progress.complete();
                }
            });

            // Save every progress-observable in a map of all observables
            status[file.name] = {
                progress: progress.asObservable()
            };
        });
        // return the map of progress.observables
        return status;
    }

    getBufferedFiles () {
        return this.bufferedFiles;
    }

    cancelSubmission() {
        this.files = [];
        this.filesListener.next([]);
        this.uploadedFiles = [];
        this.comments = '';
        this.router.navigate(['../upload/files']);
    }

    getIsUploadedSuccessfully() {
        return this.uploadedFiles.length > 0;
    }

    // submit assignment
    submitAssignment() {
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type':  'application/json',
                'withCredentials': 'true'
            })
        };

        const submitData = {
            'student_id': this.authService.getUserID(),
            'student_name': this.authService.getUserDetails()['username'],
            'assignment_id': this.utilService.getAssignmentId(),
            'subject_id': this.utilService.getCourseId(),
            'files': this.uploadedFiles,
            'comment': this.comments,
        };
        this.http.post<any>(this.utilService.getBaseUrl() + '/submissions', submitData, httpOptions)
            .subscribe(response => {
                if (response['submission_id']) {
                    this.submissionId = response['submission_id'];
                    console.log(response);
                    this.router.navigate(['../upload/success']);
                }
            }, error => {
            });
    }

    goBackHome () {
        this.files = [];
        this.filesListener.next([]);
        this.uploadedFiles = [];
        this.comments = '';
        this.router.navigate(['../upload']);
    }

    // check whether user has submitted a file or not
    checkPrev() {
        this.loadingListener.next(true);
        this.http.get(this.utilService.getBaseUrl() + '/submission/prev/check?student_id=' + this.authService.getUserID()
        + '&assignment_id=' + this.utilService.getAssignmentId())
            .subscribe(response => {
                this.hasPrevListener.next(response['result']);
                this.loadingListener.next(false);
            });
    }

    checkPreviousWork() {
        this.router.navigate(['upload/previous']);
    }

    // should return a list of files (string) from previous submission
    getPreviousSubmission() {
        this.loadingListener.next(true);
        this.http.get(this.utilService.getBaseUrl() + '/submission/prev?student_id=' + this.authService.getUserID()
            + '&assignment_id=' + this.utilService.getAssignmentId())
        .subscribe(response => {
            const prev_files: FileDataModel[] = [];
            response['files'].forEach(fileName => {
                prev_files.push({
                    fileName: fileName,
                    type: null,
                    data: null
                });
            });
            this.fileListListener.next(prev_files);
            this.loadingListener.next(false);
        });
    }

    // get file from user based on filename
    // should return a json object as following:
    // {
    //     index: String,
    //     type: String
    //     data: String
    // }
    getBufferedFile(index, fileName) {
        this.loadingListener.next(true);
        this.http.get(this.utilService.getBaseUrl() + '/file/student?filename=' + fileName, { responseType: 'blob'})
            .subscribe(file => {
                const reader = new FileReader();
                if (file.type === 'application/pdf') {
                    reader.readAsArrayBuffer(file);
                } else if (file.type === 'text/plain') {
                    reader.readAsText(file);
                }
                reader.onloadend = () => {
                    this.bufferedFileListener.next({
                        index: index,
                        type: file.type,
                        data: reader.result,
                    });
                    this.loadingListener.next(false);
                };
            });
    }
}
