import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FlexLayoutModule } from '@angular/flex-layout';

import { PdfViewerModule } from 'ng2-pdf-viewer';

import { StudentMaterialModule } from './student-material.module';
import { SharedModule } from '../shared/shared.module';

import { StudentComponent } from './student.component';
import { PreviewComponent } from './preview/preview.component';
import { UploadSuccessComponent } from './upload-success/upload-success.component';
import { MultiUploadComponent } from './multi-upload/multi-upload.component';
import { UploadHomeComponent } from './upload-home/upload-home.component';
import { UploadDialogComponent } from './upload-dialog/upload-dialog.component';
import { PreviousSubmissionComponent } from './previous-submission/previous-submission.component';

import { UploadService } from './upload.service';

/**
 * This Module include everything related to the student sit
 * If you want to update on student site
 * You should update under the /student folder and add/remove components and module in this file
 */

@NgModule({
  imports: [CommonModule,
            FlexLayoutModule,
            HttpClientModule,
            BrowserAnimationsModule,
            PdfViewerModule,
            RouterModule,
            FormsModule,
            StudentMaterialModule,
            SharedModule
          ],
  declarations: [
    StudentComponent,
    PreviewComponent,
    UploadSuccessComponent,
    MultiUploadComponent,
    UploadHomeComponent,
    UploadDialogComponent,
    PreviousSubmissionComponent],
  exports: [StudentComponent],
  entryComponents: [StudentComponent, UploadDialogComponent],
  providers: [UploadService]
})
export class StudentModule {}
