import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { forkJoin, Subscription } from 'rxjs';

import { MatDialog, MatSnackBar } from '@angular/material';

import { UploadService } from '../upload.service';
import { UploadDialogComponent } from '../upload-dialog/upload-dialog.component';

@Component({
  selector: 'app-multi-upload',
  templateUrl: './multi-upload.component.html',
  styleUrls: ['./multi-upload.component.scss']
})
export class MultiUploadComponent implements OnInit, OnDestroy {

  @ViewChild('file') file;

  public files: File[] = [];
  public comments: String = 'Default comments.';


  progress;
  canBeClosed = true;
  primaryButtonText = 'Upload';
  uploading = false;
  uploadSuccessful = false;
  filesListenerSub: Subscription;

  constructor(public uploadService: UploadService, private router: Router, public dialog: MatDialog, private _snackBar: MatSnackBar) {}


  onFilesAdded() {
    const invalids = this.uploadService.addFiles(this.file.nativeElement.files);
    // if any invalid files contained, show snack bar to user to show them requirement of upload file.
    if (invalids[0] || invalids[1]) {
      let message = invalids[0] ? 'Only PDF and TXT file are supported.     ' : '';
      message += invalids[1] ? 'Maximum file size is 5MB.' : '';
      this._snackBar.open(message, 'close', {
        duration: 10000,
      });
    }
  }

  removeFile(i) {
    this.uploadService.removeFile(i);
  }

  addFiles() {
    this.file.nativeElement.click();
  }

  // start uploading file to the server
  upload() {

    // set the component state to "uploading"
    this.uploading = true;

    // start the upload and save the progress map
    this.progress = this.uploadService.upload(this.files, this.comments);

    // convert the progress map into an array
    const allProgressObservables = [];
    // tslint:disable-next-line:forin
    for (const key in this.progress) {
      allProgressObservables.push(this.progress[key].progress);
    }

    // The OK-button should have the text "Finish" now
    this.primaryButtonText = 'Next';


    console.log(allProgressObservables);
    // When all progress-observables are completed...
    forkJoin(allProgressObservables).subscribe(end => {
      // ... the upload was successful...
      this.uploadSuccessful = true;

      // ... and the component is no longer uploading
      this.uploading = false;

      // jump to preview page
      this.router.navigate(['../upload/preview']);
    });
  }

  backToHome() {
    // if no file is selected, go back directly
    if (this.files.length === 0) {
      this.uploadService.goBackHome();
      return;
    }

    // if some files are selected, prompt user to check
    // whether to go back
    const dialogRef = this.dialog.open(UploadDialogComponent, {
      width: '250px',
      data: 'Home'
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

  ngOnInit() {
    this.filesListenerSub = this.uploadService.getFileListener()
      .subscribe(files => {
        // add logic for guards
        this.files = files;
      });
  }

  ngOnDestroy() {
    this.filesListenerSub.unsubscribe();
  }
}
