import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';

import { WorkshopModel } from './shared/model/workshopModel';

import { SidebarService } from './admin/layouts/sidebar/sidebar.service';
import { AuthService } from './shared/service/auth.service';
import { UtilService } from './shared/service/util.service';
import { DataService } from './admin/data.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  loading;
  workshops: WorkshopModel[];
  userDetails = null;
  availableWorkshopsStatusListenerSub: Subscription;
  availableWorkshopsListenerSub: Subscription;
  authStatusListenerSub: Subscription;
  authLoadingListener: Subscription;
  constructor(private authService: AuthService,
              private sidebarService: SidebarService,
              private utilService: UtilService,
              private dataService: DataService)  {}

  ngOnInit() {
    this.authLoadingListener = this.authService.getAuthLoadingListener()
      .subscribe(loading => {
        this.loading = loading;
      });
      this.availableWorkshopsStatusListenerSub = this.sidebarService.getAvailableWorkshopsStatusListener()
      .subscribe(val => {
        this.loading = val;
      });
    this.availableWorkshopsListenerSub = this.sidebarService.getAvailableWorkshopsListener()
      .subscribe(val => {
        this.workshops = val;
      });
    this.authStatusListenerSub = this.authService.getAuthStatusListener()
      .subscribe(authenticated => {
        if (!authenticated) {
          return;
        }
        this.userDetails = this.authService.getUserDetails();
        if (this.userDetails['enrollmentType'] === 'TaEnrollment') {
          this.sidebarService.getAvailableWorkshops();
        } else if (this.userDetails['enrollmentType'] === 'TeacherEnrollment') {
          this.dataService.getChartData(undefined);
        }
      });
    this.utilService.calculateIdFromUrl();
    this.authService.ltiAuthentication();
  }
}
