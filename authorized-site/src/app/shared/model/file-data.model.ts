// defined model for the use of previous submission objects
export class FileDataModel {
    type: string;
    fileName: string;
    data: string | ArrayBuffer;
}

