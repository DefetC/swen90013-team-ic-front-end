// defined model for the use of workshops in sidebar.
export class WorkshopModel {
    workshopId: string;
    workshopName: string;
    selected: boolean;
}

