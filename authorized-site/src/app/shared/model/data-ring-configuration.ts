// defined model for the use of d3 ring in chart view.
export class DataRingConfiguration {
    dataType: string;
    dimension: number;
    description: string;
}
