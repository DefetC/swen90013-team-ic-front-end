import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from './service/auth.service';

@Injectable({providedIn: 'root'})
export class TeacherGuard implements CanActivate {
    constructor(private authService: AuthService, private router: Router) { }

    canActivate(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ): boolean | Observable<boolean> | Promise<boolean>  {
        const isTeacher = this.authService.isTutor() || this.authService.isInstructor();
        if (!isTeacher) {
            this.router.navigate(['student/']);
        }
        return isTeacher;
    }
}


