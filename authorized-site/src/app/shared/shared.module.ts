import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { SpinnerModule } from './spinner/spinner.module';
import { UtilService } from './service/util.service';
import { AuthService } from './service/auth.service';

/**
 * This Module include everything shared for different sites
 * If you want to update something related to all sites
 * You should update under the /shared folder and add/remove components and module in this file
 */

@NgModule({
  imports: [
    CommonModule,
    SpinnerModule,
    BrowserAnimationsModule,
  ],
  declarations: [],
  exports: [SpinnerModule, BrowserAnimationsModule],
  providers: [UtilService, AuthService],
})
export class SharedModule {}
