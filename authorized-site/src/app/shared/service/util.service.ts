import { Injectable } from '@angular/core';

import { environment } from '../../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class UtilService {

    private courseId;
    private assignmentId;

    constructor() { }

    // retrieve course id
    getCourseId() {
        return this.courseId;
    }

    // retrieve assignment id
    getAssignmentId() {
        return this.assignmentId;
    }

    getBaseUrl() {
        return environment.baseUrl;
    }

    // different case when deal with development and production
    getAuthUrl(route) {
        if (!environment.production) {
            return environment.baseAuthUrl + route + '.json';
        } else {
            return this.getBaseUrl() + route;
        }
    }

    // get courseId and assignmentId from URL from browser in production
    // and use hard coded URL in development
    calculateIdFromUrl() {
        // get current window's parent's url given that the app is an iFrame app
        let link = (window.location !== window.parent.location)
            ? document.referrer
            : document.location.href;
        // if it is not production, use hardcoded url as there is no valid url to be used from windows
        if (!environment.production) {
            link = environment.windowUrl;
        }
        link = link.split('?')[0];
        const vals = link.split('/');
        // save courseId and assignmentId for future usage
        this.courseId = vals[4];
        this.assignmentId = vals[6];
    }

}
