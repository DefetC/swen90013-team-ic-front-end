// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  // the local server's api address
  baseUrl: 'http://localhost:5000',
  // the link to local json file to give authentication without lti protocol
  baseAuthUrl: '../assets/data',
  // the window url used for dev
  // let develop to change courseId and assignmentId
  // http://165.22.220.225:3000/courses/<courseId>/assignments/<AssignmentId>'
  windowUrl: 'http://165.22.220.225:3000/courses/1/assignments/11?module_item_id=22'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
