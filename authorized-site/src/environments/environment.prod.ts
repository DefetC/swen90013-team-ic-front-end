export const environment = {
  production: true,
  // the base url when the server is deployed
  baseUrl: 'http://localhost:5000',
  baseAuthUrl: 'http://localhost:5000',
  windowUrl: ''
};
