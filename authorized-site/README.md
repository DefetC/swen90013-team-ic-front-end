# SWEN90013-TEAM-IC-FRONT-END

This repo is about the front-end part of Unimelb SWEN90013 (Masters Advanced Software Project) Integrity checking project.
This website is built using Angular library along with SCSS package.


This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.1.1.

## Environment Setup

Run `npm install` for installing dependency of the projecet.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Simple file-receiving Node.js backend

Run `node server/server.js` to run the sever
Saved file will be in the folder server/files

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).


## Reference

File Uploading: https://malcoded.com/posts/angular-file-upload-component-with-express/
